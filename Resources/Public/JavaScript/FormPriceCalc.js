/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 01.06.16
 * Time: 09:28
 */

var FormPriceCalc = function() {

};

FormPriceCalc.prototype = {
    extendexBooking: null,
    inputNames: {
        extendexBooking: 'tx_hansesailbooking_hansesailbooking[extendexBooking]',
        paticipants: 'tx_hansesailbooking_hansesailbooking[booking][paticipants]',
        children: 'tx_hansesailbooking_hansesailbooking[booking][children]',
        type: 'tx_hansesailbooking_hansesailbooking[booking][type]'
    },
    shipping: {
        1: {
            price: 4.5,
            name: 'Barzahlung vor Ort, Tickets und Rechnung bei Abholung',
            shipment: 'Abholung',
            paymentService: false
        },
        2: {
            price: 4.5,
            name: 'EC-Kartenzahlung vor Ort, Tickets und Rechnung bei Abholung',
            shipment: 'Abholung',
            paymentService: false
        },
        3: {
            price: 4,
            name: 'Servicegebühr',
            shipment: 'EMail',
            paymentService: 'sofort'
        },
        4: {
            price: 4,
            name: 'Servicegebühr',
            shipment: 'Post',
            paymentService: 'sofort'
        },
        5: {
            price: 4,
            name: 'Servicegebühr',
            shipment: 'Post',
            paymentService: false
        },
        6: {
            price: 4,
            name: 'Servicegebühr',
            shipment: 'EMail',
            paymentService: false
        },
        default: 6,
        reservation: {
            price: 0,
            name: 'Reservierung',
            shipment: 'EMail',
            paymentService: false
        },
    },

    isExtendedBooking: function() {
        if (this.extendexBooking === null) {
            this.extendexBooking = this.getInputValue(this.inputNames.extendexBooking) == "1";
        }
        return this.extendexBooking
    },

    getPriceObject: function() {

        var priceObject = {
            total: 0,
            totalFormated: '0,00',
            lineItems: []
        };

        var price = 0;
        var totalPrice = 0;
        var type = 0;
        var shipping = {};

        if (!this.isExtendedBooking()) {
            var adults = this.getInputValue(this.inputNames.paticipants);
            var children = this.getInputValue(this.inputNames.children);
        } else {
            var adults = this.getInputValue(this.inputNames.paticipants);
            var children = $('.childcheckbox').length();
            adults = adults - children;
        }

        // Adults
        price = parseFloat(this.getProductValue('price'));
        totalPrice = price*adults;

        priceObject.total += totalPrice;

        priceObject.lineItems.push({
            name: 'Erwachsene',
            qty: adults,
            price: price,
            priceFormated: this.numberFormat(price, 2, ',', '.'),
            totalPrice: totalPrice,
            totalPriceFormated: this.numberFormat(totalPrice, 2, ',', '.')
        });

        // Children
        if (children > 0) {
            price = parseFloat(this.getProductValue('price_kids_c'));
            if (price <= 0) {
                price = parseFloat(this.getProductValue('price'));
            }
            totalPrice = price*children;

            priceObject.total += totalPrice;
            priceObject.lineItems.push({
                name: 'Kinder',
                qty: children,
                price: price,
                priceFormated: this.numberFormat(price, 2, ',', '.'),
                totalPrice: totalPrice,
                totalPriceFormated: this.numberFormat(totalPrice, 2, ',', '.')
            });
        }

        //Shipping

        if ($('[name="'+this.inputNames.type+'"]').length == 0) {
            shipping = false;
        } else {
            type = this.getInputValue(this.inputNames.type);
            if (typeof this.shipping[type] == 'object') {
                shipping = this.shipping[type];
            } else {
                shipping = this.shipping[this.shipping.default];
            }
        }
        if (noservice) shipping = false;
        if (shipping) {
            priceObject.total += shipping.price;
            priceObject.lineItems.push({
                name: shipping.name,
                qty: 1,
                price: shipping.price,
                priceFormated: this.numberFormat(shipping.price, 2, ',', '.'),
                totalPrice: shipping.price,
                totalPriceFormated: this.numberFormat(shipping.price, 2, ',', '.')
            });
        }
        priceObject.totalFormated = this.numberFormat(priceObject.total, 2, ',', '.');
        return priceObject;
    },

    getInputValue: function(inputName) {
        var input = $('[name="'+inputName+'"]');
        if (input.length == 0) {
            throw "Could not find input '"+inputName+"'";
        }
        if (input.attr('type') == 'radio') {
            input = $('[name="'+inputName+'"]:checked');
        }

        return input.val();
    },

    getProductValue: function(keyName) {
        var product = aos_product;
        if (typeof product != 'object') {
            throw "Could not find object aos_product in scope";
        }
        if (typeof product[keyName] == "undefined") {
            throw "Key '"+keyName+"' does not exist in aos_product";
        }
        return product[keyName];
    },

    numberFormat: function(number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
};