
plugin.tx_hansesail_booking {
	view {
		templateRootPath = {$plugin.tx_hansesail_booking.view.templateRootPath}
		partialRootPath = {$plugin.tx_hansesail_booking.view.partialRootPath}
		layoutRootPath = {$plugin.tx_hansesail_booking.view.layoutRootPath}
		widget.TYPO3\CMS\Fluid\ViewHelpers\Widget\PaginateViewHelper.templateRootPath = {$plugin.hansesail_booking.view.templateRootPath}
	}
	persistence {
	storagePid = {$plugin.hansesail_booking.persistence.storagePid}
        classes {

        }
    }
	features {

	}
}

plugin.tx_reisedb._CSS_DEFAULT_STYLE (

)