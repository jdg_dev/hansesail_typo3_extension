/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 10.02.16
 * Time: 13:55
 */

var HanseSailBookingList = function(products) {
    this.start = new Date().getTime();
    this.hash = location.hash.substr(1)
        .replace('[_]', ' ')
        .replace('[AE]', 'Ä')
        .replace('[OE]', 'Ö')
        .replace('[UE]', 'Ü')
        .replace('[ae]', 'a')
        .replace('[oe]', 'ö')
        .replace('[ue]', 'ü')
        .replace('[ss]', 'ß')
        .split(':');
    this.defaultMinDate = moment('05.08.2021', 'DD.MM.YYYY');
    this.defaultMaxDate = moment('08.08.2021', 'DD.MM.YYYY');

    /**
     * Initialize the Collection
     */
    this.initCollection(products);

    /**
     * Find minimal an maximal date, this is used for the maximal Daterange of the Datepicker
     */
    this.minDate = moment(this.products.min(function(model) {
        var timestamp = parseInt(moment(model.get('datum_c'), 'YYYY-MM-DD').format('X'));
        if (_.isNaN(timestamp)) timestamp = 9000000000;
        return timestamp;
    }).get('datum_c'), 'YYYY-MM-DD');
    this.maxDate = moment(this.products.max(function(model) {
        var timestamp = parseInt(moment(model.get('datum_c'), 'YYYY-MM-DD').format('X'));
        if (_.isNaN(timestamp)) timestamp = 0;
        return timestamp;
    }).get('datum_c'), 'YYYY-MM-DD');

    /**
     * Create the eventsPerDay object. Set for each day 0 Events
     * @type {{}}
     */
    this.eventsPerDay = {};

    this.loopDay = moment(this.products.min(function(model) {
        return moment(model.get('datum_c'), 'YYYY-MM-DD').format('X')
    }).get('datum_c'), 'YYYY-MM-DD');
    do {
        this.eventsPerDay[this.loopDay.format('YYYYMMDD')] = 0;
        this.loopDay.add(1, 'days');
    } while (this.loopDay <= this.maxDate);

    /**
     * Initial Shiptype Array
     * @type {Array}
     */
    this.types = [];

    /**
     * Initial Shipnames Array
     * @type {Array}
     */
    this.names = [];

    /**
     * Scan every product do some more init
     */

    this.products.each(function(product){
        /**
         * Put this one in the right place on the eventsPerDay object
         */
        var modelDate = product.get('datum_c');
        this.eventsPerDay[moment(modelDate, 'YYYY-MM-DD').format('YYYYMMDD')]++;

        /**
         * Create a List of all Shiptypes
         */
        var type = product.get('hs_schiffe_aos_products_1')[0].category;
        if (_.indexOf(this.types, type) < 0) {
            this.types.push(type);
        }

        /**
         * Create a List of all Shipnames
         */
        var name = product.get('hs_schiffe_aos_products_1')[0].name;
        if (_.indexOf(this.names, name) < 0 && _.isString(name) && name.length > 0) {
            this.names.push(name);
        }
    }, this);

    /**
     * Sort Shiptypes array
     */
    this.types = _.sortBy(this.types, function(type) { return type});

    /**
     * Sort Shipnames array
     */
    this.names = _.sortBy(this.names, function(type) { return type});

    /**
     * fill the Shiptype Filter-Selectbox
     */
    for(var i in this.types) {
        $('#vesseltype').append('<option value="'+this.types[i]+'">'+this.types[i]+'</option>');
    }

    /**
     * fill the Shipname Filter-Selectbox
     */
    for(var i in this.names) {
        $('#vesselname').append('<option value="'+this.names[i]+'">'+this.names[i]+'</option>');
    }
    if (this.hash[0] == 's') {
        $('#vesselname').val(this.hash[1]);
    }


    /**
     * Add the Action for the Shiptype Filter-Selectbox
     */
    $('#vesseltype').change(_.bind(function(e) {
        if ($('#vesselname').val() != '') $('#vesselname').val('');
        this.updateView();
    }, this));

    /**
     * Add the Action for the Shipname Filter-Selectbox
     */
    $('#vesselname').change(_.bind(function(e) {
        if ($('#vesseltype').val() != '') $('#vesseltype').val('');
        this.updateView();
    }, this));

    /**
     * Set minmal and maximal date as the Startvalues for the Daterange-Filter
     */
    var defaultMinDate = this.defaultMinDate.format('DD.MM.YYYY');
    var defaultMaxDate = this.defaultMaxDate.format('DD.MM.YYYY');

    if (this.hash[0] == 'd') {
        var hashMomentMin = moment(this.hash[1], 'DD.MM.YYYY');
        var hashMomentMax = moment(this.hash[2], 'DD.MM.YYYY');

        if (hashMomentMin.isSameOrAfter(this.minDate, 'day') && hashMomentMin.isSameOrBefore(this.maxDate, 'day')) {
            defaultMinDate = hashMomentMin.format('DD.MM.YYYY');
        }
        if (hashMomentMax.isSameOrAfter(this.minDate, 'day') && hashMomentMax.isSameOrBefore(this.maxDate, 'day')) {
            defaultMaxDate = hashMomentMax.format('DD.MM.YYYY');
        }
    }

    console.log(this.minDate);
    console.log(this.maxDate);

    $('#daterange').find('input[name="start"]').val(defaultMinDate);
    $('#daterange').find('input[name="end"]').val(defaultMaxDate);

    /**
     * Initialize the Daterange-Filter Datepicker
     */
    $('#daterange').datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        startDate: this.minDate.format('DD.MM.YYYY'),
        endDate: this.maxDate.format('DD.MM.YYYY'),
        maxViewMode: 0,
        autoclose: true,
        language: "de",
        beforeShowDay: _.bind(function (date){
            if (this.eventsPerDay[moment(date).format('YYYYMMDD')] > 0) {
                return {
                    tooltip: this.eventsPerDay[moment(date).format('YYYYMMDD')]+' '+(this.eventsPerDay[moment(date).format('YYYYMMDD')] == 1 ? 'Ausfahrt':'Ausfahrten'),
                    classes: 'hasEvents bg-info'
                };
            }
        }, this)
    }).on('changeDate', _.bind(function(e) {
        this.updateView();
    }, this));

    /**
     * Render the View
     */
    this.initView();

};

HanseSailBookingList.prototype = {

    /**
     * Just for debugging, stops the time since start or last lap() and put it together with msg in the debug console.
     * @param msg
     */
    lap: function(msg) {
        var end = new Date().getTime();
        console.log(msg + ': ' + (end - this.start));
        this.start = end;
    },

    /**
     * Creates the View Objects
     */
    initView: function() {

        /**
         * View for a single Product
         */
        var AosProductView = Backbone.View.extend({

            events: {
                'click .moreInfos': 'toggleMoreInfos'
            },

            toggleMoreInfos: function(e) {
                e.preventDefault();
                var mobileInfo = this.$el.find('.mobileInfo');
                /**
                 * If the Mobile info not exist, create one
                 */
                if (mobileInfo.length == 0) {
                    var mobileInfo = $('<div class="mobileInfo col-xs-12"></div>');
                    mobileInfo.html(this.$el.find('.moreInfosSource').html()).hide();
                    this.$el.find('.ship_trip_info').append(mobileInfo);
                }

                if (mobileInfo.is(':visible')) {
                    mobileInfo.slideUp('fast');
                    this.$el.find('.moreInfos').text('Mehr Infos...');
                } else {
                    mobileInfo.slideDown('fast');
                    this.$el.find('.moreInfos').text('Weniger Infos...');
                }

            },

            template: _.template( $('#list_template').html()),

            /**
             * Render view on init
             */
            initialize: function(){
                this.render();
            },

            /**
             * Render the view
             * @returns {AosProductView}
             */
            render: function(){
                model = this.model.toJSON();
                model.moment = moment;
                this.$el.html( this.template(model));
                return this;
            }
        });

        /**
         * View for the Product List
         */
        var AosProductsView = Backbone.View.extend({

            /**
             * Last id of the ship that is rendered last
             */
            lastShipId: '',

            /**
             * Counter for lazyLoad iterations
             */
            lastLazyLoadIndex: 0,

            /**
             * How many Products should be rendered in on lazyLoad iteration
             */
            lastLazyLoadItems: 10,

            /**
             * Toggle the scroll watchdog that triggers the lazyLoad
             */
            watchScrollEvents: false,

            /**
             * Render view on init
             */
            initialize: function(){
                this.render();
            },

            /**
             * Gets a new collection and renders the result in the view. e.g. after a filter action
             * @param collection
             */
            updateCollection: function(collection) {
                this.lastShipId = '';
                this.collection = collection;
                this.render(0);
            },

            /**
             * Triggers the next lazyLoad iteration.
             */
            lazyLoad: function() {
                this.render(this.lastLazyLoadIndex + 1);
            },

            /**
             * Renders the view, index is the lazyLoad iteration
             * @param index
             */
            render: function(index){
                this.watchScrollEvents = false;
                if (!index) index = 0;
                this.lastLazyLoadIndex = index;
                var renderElements = _.bind(function() {
                    if (this.lastLazyLoadIndex == 0) this.$el.empty();
                    var c = 0;
                    this.collection.each(function(product){
                        if (c >= this.lastLazyLoadIndex*this.lastLazyLoadItems && c < (this.lastLazyLoadIndex+1)*this.lastLazyLoadItems) {
                            product.set('showShip', (this.lastShipId != product.get('hs_schiffe_aos_products_1')[0].id));
                            this.lastShipId = product.get('hs_schiffe_aos_products_1')[0].id;

                            var productView = new AosProductView({ model: product,});
                            this.$el.append(productView.render().$el);
                        }
                        c++;
                    }, this);
                }, this);
                if (index == 0) {
                    this.$el.fadeOut('fast', renderElements);
                    this.$el.fadeIn('fast', _.bind(function() {
                        this.watchScrollEvents = true;
                    },this));
                } else {
                    renderElements();
                    this.watchScrollEvents = true;
                }

            }
        });

        /**
         * Init the View
         */

        var shiptype = $('#vesseltype').val();
        var shipname = $('#vesselname').val();
        var minDate = $('#daterange').find('input[name="start"]').val();
        var maxDate = $('#daterange').find('input[name="end"]').val();

        var products = this.products.setFilter(shiptype, minDate, maxDate, shipname);

        this.view = new AosProductsView({ el: $('#list_target'), collection: products });

        /**
         * lazyLod scroll wachdog, triggers lazyload if allowed by the view and if document end is reached
         */
        $(window).scroll(_.throttle(_.bind(function() {
            if (this.view.watchScrollEvents) {
                var scroll = $(window).scrollTop()+$(window).height();
                var listOffset = $('#list_target').offset();
                var listHeight = $('#list_target').height();
                if (scroll >= (listOffset.top+listHeight)) {
                    this.view.lazyLoad();
                }
            }
        }, this), 100));
    },

    /**
     * Collect filter settings, generate a new collection an put it on the view
     */
    updateView: function() {
        var shiptype = $('#vesseltype').val();
        var shipname = $('#vesselname').val();
        var minDate = $('#daterange').find('input[name="start"]').val();
        var maxDate = $('#daterange').find('input[name="end"]').val();

        var products = this.products.setFilter(shiptype, minDate, maxDate, shipname);

        this.view.updateCollection(products);
    },

    /**
     * Creates the Collection from the aos_products json.
     * @param products
     */
    initCollection: function(products) {

        /**
         * Model Definition
         */
        var AosProduct = Backbone.Model.extend({

        });

        /**
         * Colection Definition
         */
        var AosProducts = Backbone.Collection.extend({
            model: AosProduct,

            /**
             * INIT
             * @param models
             * @param options
             */
            initialize: function (models,options) { },

            /**
             * This sets the sorting of the Collection.
             * @param item
             * @returns {*}
             */
            comparator: function(item) {
                return item.get('hs_schiffe_aos_products_1')[0].name + item.get("datum_c");
            },

            /**
             * Filters the collection
             * @param shiptype
             * @param minDate
             * @param maxDate
             * @param shipname
             * @returns {*}
             */
            setFilter: function (shiptype, minDate, maxDate, shipname) {
                filtered = this.filter(function (item) {
                    if (shiptype == '') {
                        isShipType = true;
                    } else {
                        isShipType = (item.get('hs_schiffe_aos_products_1')[0].category == shiptype);
                    }

                    if (shipname == '') {
                        isShipName = true;
                    } else {
                        isShipName = (item.get('hs_schiffe_aos_products_1')[0].name == shipname);
                    }


                    isMinDate = (moment(item.get('datum_c'), 'YYYY-MM-DD').format('YYYYMMDD') >= moment(minDate, 'DD.MM.YYYY').format('YYYYMMDD'));
                    isMaxDate = (moment(item.get('datum_c'), 'YYYY-MM-DD').format('YYYYMMDD') <= moment(maxDate, 'DD.MM.YYYY').format('YYYYMMDD'));
                    return (isShipType && isMinDate && isMaxDate && isShipName);
                });
                return new AosProducts(filtered);
            },

            /**
             * Returns all Products with a future eventdate
             * @returns {*}
             */
            futureEvents: function () {
                filtered = this.filter(function (item) {
                    var modelDate = item.get('datum_c');
                    if (!/^20[1-9][0-9]-[0-1][0-9]-[0-3][0-9]$/.test(modelDate)) {
                        return false;
                    }
                    return moment(modelDate, 'YYYY-MM-DD').isAfter(moment());
                });
                return new AosProducts(filtered);
            }
        });

        /**
         * Init the Collection, put only future events in the Collection
         */
        //this.products = new AosProducts(products).futureEvents();
        this.products = new AosProducts(products);
    }
};

/**
 * Start the App
 */
var hanseSailBookingList = false;
function r(f){(/in/.test(document.readyState) || typeof $ != 'function' || typeof _ != 'function')?setTimeout(r,9,f):f()}
r(function(){
    if (_.isArray(aos_products.entry_list)) {
        hanseSailBookingList = new HanseSailBookingList(aos_products.entry_list);
    }
});
