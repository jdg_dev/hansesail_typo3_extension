<?php
namespace Sofort\SofortLib;

require __DIR__ . '/../vendor/autoload.php';

// Enter your configuration key.
// You only can create a new configuration key by creating a new Gateway project in your account at sofort.com
$configkey = '82894:200686:4fafa6b7fe6966d909be587feb868478';

$Sofortueberweisung = new Sofortueberweisung($configkey);

$Sofortueberweisung->setAmount(10.21);
$Sofortueberweisung->setCurrencyCode('EUR');
$Sofortueberweisung->setReason('Testueberweisung', 'Verwendungszweck');
$Sofortueberweisung->setSuccessUrl('https://www.jakota.de', true); // i.e. http://my.shop/order/success
$Sofortueberweisung->setAbortUrl('https://www.jakota.de');
// $Sofortueberweisung->setSenderSepaAccount('SFRTDE20XXX', 'DE06000000000023456789', 'Max Mustermann');
// $Sofortueberweisung->setSenderCountryCode('DE');
// $Sofortueberweisung->setNotificationUrl('YOUR_NOTIFICATION_URL', 'loss,pending');
// $Sofortueberweisung->setNotificationUrl('YOUR_NOTIFICATION_URL', 'loss');
// $Sofortueberweisung->setNotificationUrl('YOUR_NOTIFICATION_URL', 'pending');
// $Sofortueberweisung->setNotificationUrl('YOUR_NOTIFICATION_URL', 'received');
// $Sofortueberweisung->setNotificationUrl('YOUR_NOTIFICATION_URL', 'refunded');
$Sofortueberweisung->setNotificationUrl('https://www.jakota.de');
//$Sofortueberweisung->setCustomerprotection(true);


$Sofortueberweisung->sendRequest();

if($Sofortueberweisung->isError()) {
    // SOFORT-API didn't accept the data
    echo $Sofortueberweisung->getError();
} else {
    // get unique transaction-ID useful for check payment status
    $transactionId = $Sofortueberweisung->getTransactionId();
    // buyer must be redirected to $paymentUrl else payment cannot be successfully completed!
    $paymentUrl = $Sofortueberweisung->getPaymentUrl();
    header('Location: '.$paymentUrl);
}
