<?php
namespace JAKOTA\HansesailBooking\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (TYPO3_MODE == 'BE') {
    require_once('../../../Resources/PHP/tcpdf/tcpdf.php');
    require_once('../../../Resources/PHP/fpdi/fpdi.php');
} else {
    require_once('typo3conf/ext/hansesail_booking/Resources/Private/PHP/tcpdf/tcpdf.php');
    require_once('typo3conf/ext/hansesail_booking/Resources/Private/PHP/fpdi/fpdi.php');
}

class TemplateTCPDF extends \FPDI {

    public function __construct() {
        parent::__construct();
    }

    //Page header
    public function Header() {

    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'B', 6);
        // Page number

        //$this->Cell(0, 12, 'http://www.hansesail.com | Stand: '.date('d.m.Y H.i').' Uhr | Seite: '. $this->getAliasNumPage().' / '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');

        $this->SetFont('helvetica', '', 6);

        $this->Image($_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/hansesail_booking/Resources/Public/img/Logo_Hanse_Sail_Verein.jpg',
            20, 273, $w = 15, $h = 0, $type = '', $link = '', $align = '', $resize = true
        );
        $this->WriteHTMLCell(0,0,40,273,'<table cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td valign="top" rowspan="2"><strong>Hanse Sail Verein z. Förd.<br>trad. Schifffahrt in d. Ostsee e.V.</strong><br />
                Tall-Ship Buchungszentrale<br />
                Warnowufer 65<br />
                DE 18057 Rostock
            </td>
            <td valign="top">E-Mail: tallshipbooking@gmx.de<br />
                Telefon: +49 381 - 381 29 75/76<br />
            </td>
            <td valign="top">Öffnungszeiten:<br />
                Mo. - Fr. 8.30 - 12:00 Uhr<br />
                12:30 - 17.00 Uhr<br />
            </td>
        </tr>
        <tr>
            <td valign="bottom">Stand: '.date('d.m.Y H.i').' Uhr</td>
            <td valign="bottom">Seite: '. $this->getAliasNumPage().' von '.$this->getAliasNbPages().'</td>
        </tr>
        </table>');
    }

}