<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hansesail_booking"
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Hansesail Booking Extension',
	'description' => 'Lists of bookable events and bookingform',
	'category' => 'be',
	'author' => 'Martin Fünning',
	'author_email' => 'fuenning@jakota.de',
	'author_company' => 'JAKOTA Design Group GmbH',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '1.2.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2',
			'static_info_tables' => '6.2.1',
			'static_info_tables_de' => '6.2'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);