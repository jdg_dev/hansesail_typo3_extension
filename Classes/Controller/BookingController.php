<?php
namespace JAKOTA\HansesailBooking\Controller;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/
/**
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class BookingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * crmConnector
     *
     * @var \JAKOTA\HansesailBooking\Utility\SuiteCrmConnector
     * @inject
     */
    protected $crmConnector = NULL;

    /**
     * CRM ID of assigned User
     * default is Mandy Lange <9e4fec0d-43a3-ff9b-21e0-5641d70b1f4a>
     * for debug use Martin Fünning <e5f94862-6e7c-a201-a046-56c1c865d53d>
     *
     * @var String
     */
    protected $assignedCRMUser = '9e4fec0d-43a3-ff9b-21e0-5641d70b1f4a';

    /**
     * sofort_lib configKey
     *
     * @var String
     */
    protected $sofortConfigKey = '82894:200686:4fafa6b7fe6966d909be587feb868478';

    /**
     * pageRenderer
     *
     * @var \TYPO3\CMS\Core\Page\PageRenderer
     * @inject
     */
    protected $pageRenderer;

    /**
     * daytripRepository
     *
     * @var \AgenturVergin\Hansesail\Domain\Repository\DaytripRepository
     * @inject
     */
    protected $daytripRepository;

    /**
     * shipRepository
     *
     * @var \AgenturVergin\Hansesail\Domain\Repository\ShipRepository
     * @inject
     */
    protected $shipRepository;

    /**
     * Controller Context to use
     *
     * @var \TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext
     * @inject
     */
    protected $controllerContext;

    /**
     * Country Repository
     *
     * @var \SJBR\StaticInfoTables\Domain\Repository\CountryRepository
     * @inject
     */
    protected $countryRepository;

    /**
     * Needed Fields from the hs_schiffe crm-module
     * @var array
     */
    protected $shipFields = array(
        'id',
        'name',
        'flagge',
        'category',
        'hulk_lenght',
        'baujahr',
        'nur_reservierung_c',
        'image',
        'uid'
    );

    /**
     * Needed Fields from the aos_products crm-module
     * @var array
     */
    protected $productFields = array(
        'id',
        'name',
        'type',
        'datum_c',
        'zeit_c',
        'price',
        'restkapazitaet_c',
        'inkludierte_leistungen_c',
        'price_kids_c',
        'description',
        'liegeplatz_c',
        'category'
    );

    public function shipToProductAction() {
        $aosProductResponse = $this->crmCall(
            'get_entry_list',                   // Methode
            'AOS_Products',                     // Module Name
            'aos_products.type = "Ausfahrt"',   // Query
            '',                                 // Order BY
            0,                                  // Offset
            array(),                            // Select Fields
            array(
                array(
                    'name' => 'hs_schiffe_aos_products_1',
                    'value' => array('id', 'name', 'flagge', 'category', 'hulk_lenght', 'uid'),
                ),
            )
        );
        $products = array();
        $shipDone = array();
        foreach($aosProductResponse->body->entry_list as $key => $product) {

            preg_match('/^([A-Z0-9ÄÖÜ\- ]*)[\- ].*/', $product->name_value_list->name->value, $shipnameMatches);

            $statusShipinfo = '<span class="label label-default">Wait</span>';
            $statusProdinfo = '<span class="label label-default">Wait</span>';

            if (count($aosProductResponse->body->relationship_list[$key]->link_list[0]->records) == 0) {
                if (isset($shipnameMatches[1])) {
                    $ships = $this->crmCall(
                        'get_entry_list',
                        'HS_Schiffe',
                        'hs_schiffe.name LIKE "' . $shipnameMatches[1] . '"'
                    );
                    if ($ships->body->total_count == 1) {
                        $status = '<span class="label label-info">Writing</span>';
                        $this->crmCall(
                            'set_relationship',
                            'AOS_Products',
                            $product->name_value_list->id->value,
                            'hs_schiffe_aos_products_1',
                            array($ships->body->entry_list[0]->id)
                        );



                    } else if ($ships->body->total_count > 1) {
                        $status = '<span class="label label-warning">Too many hits</span>';
                    } else {
                        $status = '<span class="label label-danger">No hits</span>';
                    }
                } else {
                    $status = '<span class="label label-danger">No shipname found</span>';
                }
            } else {
                $status = '<span class="label label-success">OK</span>';
            }

            $products[] = array(
                'name' => $product->name_value_list->name->value,
                'shipname' => $shipnameMatches[1],
                'resopnseCount' => $status,
                'resopnseCountShipinfo' => $statusShipinfo,
                'resopnseCountProdinfo' => $statusProdinfo
            );
        }
        $this->view->assign('products', $products);

    }

    private function crmCall()
    {
        $arguments = func_get_args();
        $method = array_shift($arguments);
        try {
            return call_user_func_array(array($this->crmConnector, $method), $arguments);
        } catch (\Exception $e) {
            $this->redirect('crmError');
        }
    }

    /**
     * eID Target for Sofortüberweisung API to push transaction notifications.
     * @return string
     */
    public function sofortNotificationsAction() {
        $SofortLib_Notification = new \Sofort\SofortLib\Notification();
        $TestNotification = $SofortLib_Notification->getNotification(file_get_contents('php://input'));
        $SofortLibTransactionData = new \Sofort\SofortLib\TransactionData($this->sofortConfigKey);
        $SofortLibTransactionData->addTransaction($TestNotification);
        $SofortLibTransactionData->setApiVersion('2.0');
        $SofortLibTransactionData->sendRequest();

        $status = $SofortLibTransactionData->getStatus();
        $reason = $SofortLibTransactionData->getStatusReason();

        if ($status != '' || $reason != '') {

        }
        return '';
    }

    /**
     * Show the Sofortüberweisung successfull message
     */
    public function sofortOkAction() {
        if ($this->request->hasArgument('id')) {
            $this->sofortLog($this->request->getArgument('id'), 'Transaktion erfolgreich abgeschlossen');
            $this->crmCall('set_entry', 'AOS_Quotes', $this->crmConnector->formatNameValueList(array('id' => $this->request->getArgument('id'), 'stage' => 'Bezahlt', 'invoice_status' => 'Invoiced')));
            $this->redirect('sofortOk');
        }
    }

    private function sofortLog($quoteId, $message) {
        $finalQuote = $this->crmCall('get_entry', 'AOS_Quotes', $quoteId);
        $logtext = $finalQuote->body->entry_list[0]->name_value_list->sofort_log_c->value.date('Y.m.d H:i:s').': '.$message."\n";
        $this->crmCall('set_entry', 'AOS_Quotes', $this->crmConnector->formatNameValueList(array('id' => $quoteId, 'sofort_log_c' => $logtext)));
    }

    /**
     * Show the Sofortüberweisung abortion message
     */
    public function sofortAbortAction() {
        if ($this->request->hasArgument('id')) {
            $this->sofortLog($this->request->getArgument('id'), 'Transaktion durch User abgebrochen');
            $this->redirect('sofortAbort');
        }
    }

    /**
     * Just Show the Teaser
     */
    public function teaserAction() {
        $aosProductResponse = $this->crmCall(
            'get_entry_list',                   // Methode
            'AOS_Products',                     // Module Name
            'aos_products.type = "Ausfahrt" AND aos_products.category LIKE "Rostock"',   // Query
            '',                                 // Order BY
            0,                                  // Offset
            $this->productFields,               // Select Fields
            array(
                array(
                    'name' => 'hs_schiffe_aos_products_1',
                    'value' => $this->shipFields,
                ),
            )
        );

        $products = $this->crmConnector->removeOverhead($aosProductResponse);

        $ships = array();
        foreach($products->entry_list as $product) {
            $shipname = $product->hs_schiffe_aos_products_1[0]->name;
            if (!in_array($shipname, $ships) && $shipname != '') array_push($ships, $shipname);
        }

        asort($ships);
        $this->view->assign('ships', $ships);
        $this->view->assign('settings', $this->settings);
    }

    /**
     * List all Products and generate links to the bookingform
     */
    public function listAction()
    {

        if ($this->request->hasArgument('section')) {
            $this->view->assign('overrideSection', $this->request->getArgument('section'));
        }

        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);
        $this->daytripRepository->setDefaultQuerySettings($querySettings);

        $aosProductResponse = $this->crmCall(
            'get_entry_list',                   // Methode
            'AOS_Products',                     // Module Name
            'aos_products.type = "Ausfahrt" AND aos_products.category LIKE "Rostock"',   // Query
            '',                                 // Order BY
            0,                                  // Offset
            $this->productFields,               // Select Fields
            array(
                array(
                    'name' => 'hs_schiffe_aos_products_1',
                    'value' => $this->shipFields,
                ),
            )
        );

        $products = $this->crmConnector->removeOverhead($aosProductResponse);
        $products = $this->augumentProducts($products);
        $this->view->assign('products', $products);
        $this->view->assign('jsonProducts', json_encode($products));
    }

    private static function cmp($a, $b)
    {
        $cmp = strcmp($a->hs_schiffe_aos_products_1[0]->name, $b->hs_schiffe_aos_products_1[0]->name);
        if ($cmp == 0) {
            $cmp = strcmp($a->datum_c.$a->zeit_c, $b->datum_c.$b->zeit_c);
        }
        return $cmp;
    }

    public function pdfAction() {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);
        $this->daytripRepository->setDefaultQuerySettings($querySettings);

        $aosProductResponse = $this->crmCall(
            'get_entry_list',                   // Methode
            'AOS_Products',                     // Module Name
            'aos_products.type = "Ausfahrt" AND aos_products.category LIKE "Rostock"',   // Query
            '',                                 // Order BY
            0,                                  // Offset
            $this->productFields,               // Select Fields
            array(
                array(
                    'name' => 'hs_schiffe_aos_products_1',
                    'value' => $this->shipFields,
                ),
            )
        );

        $products = $this->crmConnector->removeOverhead($aosProductResponse);
        $products = $this->augumentProducts($products);

        $pdf = new \JAKOTA\HansesailBooking\Utility\TemplateTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Hansestadt Rostock - Tourismuszentrale Rostock & Warnemünde');
        $pdf->SetTitle('Tagesfahrten zur Hanse Sail');
        $pdf->SetSubject('Tagesfahrten zur Hanse Sail');
        $pdf->SetKeywords('HanseSail, Hanse Sail, Seegelschiff, Seegelboot, Rostock, Ostsee');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();
        $pdf->Image($_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/hansesail_booking/Resources/Public/img/Logo_Hanse_Sail.jpg',
            10, 10, $w = 40, $h = 0, $type = '', $link = '', $align = '', $resize = true
        );
         $pdf->SetFont('helvetica', 'R', 20);
        // Title
        $pdf->Text(60, 16, 'Tagesfahrten zur Hanse Sail 2020');

        $lastShip = false;
        $html = '';
        $pdf->SetFont('helvetica', 'R', 6);

        // Sort by shipname and date
        $productList = $products->entry_list;
        usort($productList, array('\\JAKOTA\\HansesailBooking\\Controller\\BookingController','cmp'));
        $imageService = $this->objectManager->get('TYPO3\CMS\Extbase\Service\ImageService');
        $ships = array();
        $dates = array();

        foreach($productList as $key => $product) {
            if (
            $product->hs_schiffe_aos_products_1[0]->name &&
            $product->datum_c &&
            strtotime($product->datum_c) >= mktime(0,0,0,8,4,2021) && strtotime($product->datum_c) <= mktime(0,0,0,8,11,2021)
            ) {
                if ($lastShip != $product->hs_schiffe_aos_products_1[0]->id && ($product->datum_c && $product->restkapazitaet_c > 0)) {
                    // Render Ship
                    /**
                     * @var $imageService \TYPO3\CMS\Extbase\Service\ImageService
                     */
                    if (is_file($product->hs_schiffe_aos_products_1[0]->image)) {
                        $image = $imageService->getImage($product->hs_schiffe_aos_products_1[0]->image, null, false);
                        $processingInstructions = array(
                            'width' => 200,
                            'height' => null,
                            'minWidth' => null,
                            'minHeight' => null,
                            'maxWidth' => null,
                            'maxHeight' => null,
                        );
                        $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
                        $image = $imageService->getImageUri($processedImage);
                    } else {
                        $image = '';
                    }

                     if (preg_match('/^\//', $image)) $image = substr($image, 1);
                     $ship = '<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr nobr="true"><td><h1>'.$product->hs_schiffe_aos_products_1[0]->name.'</h1>';
                     $ship .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">
                                 <tr>
                                 <td width="130"><img src="'.($image ? $image : 'typo3conf/ext/hansesail_booking/Resources/Public/img/default.png').'" width="120"><br />';

                     if ($product->hs_schiffe_aos_products_1[0]->category) {
                         $ship .= 'Typ: <strong>'.$product->hs_schiffe_aos_products_1[0]->category.'</strong><br />';
                     }
                     if ($product->hs_schiffe_aos_products_1[0]->hulk_lenght) {
                         $ship .= 'Länge: <strong>'.$product->hs_schiffe_aos_products_1[0]->hulk_lenght.'m</strong><br />';
                     }
                     if ($product->hs_schiffe_aos_products_1[0]->baujahr) {
                         $ship .= 'Baujahr: <strong>'.$product->hs_schiffe_aos_products_1[0]->baujahr.'</strong><br />';
                     }
                     if ($product->hs_schiffe_aos_products_1[0]->country) {
                         $ship .= 'Flagge: <strong>'.$product->hs_schiffe_aos_products_1[0]->country.'</strong><br />';
                     }
                     $ship .='</td><td width="*">{dates}</td></tr></table><hr></td></tr></table>';
                     $ships[$product->hs_schiffe_aos_products_1[0]->id] = $ship;
                     $lastShip = $product->hs_schiffe_aos_products_1[0]->id;
                }


                if ($product->datum_c && $product->restkapazitaet_c > 0) {

                    $date = '<table cellpadding="0" cellspacing="2" border="0" width="100%"><tr>';
                    $date .= '<td width="90"><strong>'.date('d.m.Y', strtotime($product->datum_c)).'<br/>'.$product->zeit_c.'<br>Liegeplatz: '.$product->liegeplatz_c.'</strong></td>';
                    $date .= '<td width="*">'.number_format(floatval($product->price),2,',','.').' / '.$product->description.' '.$product->inkludierte_leistungen_c.'</td>';
                    $date .= '</tr></table>';

                    $dates[$product->hs_schiffe_aos_products_1[0]->id][] = $date;
                }
            }
        }

        foreach($ships as $id => $shipHtml) {
            $ships[$id] = str_replace('{dates}', implode('<hr>', $dates[$id]), $shipHtml);
        }

        $pdf->WriteHTMLCell(170,0,20,40,implode('', $ships));
        if ($this->request->hasArgument('mode')) {
            if ($this->request->getArgument('mode') == 'download') {
                $pdf->Output('Hanse_Sail_'.date('Y').'_Tagesfahrten_'.date('dmY').'.pdf', 'D');
                die;
            }
        }

        $pdf->Output('Hanse_Sail_'.date('Y').'_Tagesfahrten_'.date('dmY').'.pdf', 'I');
        die;
    }

    private function augumentProducts($products) {
        foreach($products->entry_list as $key => $product) {

            if (!isset($products->entry_list[$key]->hs_schiffe_aos_products_1[0]))
                $products->entry_list[$key]->hs_schiffe_aos_products_1[0] = new \stdClass();

            // If description does not start with a price, put price_kids_c in place
            $price_kids_c = floatval($products->entry_list[$key]->price_kids_c);
            if (!preg_match('/^[0-9]/', trim($products->entry_list[$key]->description)) && $price_kids_c > 0) {
                $products->entry_list[$key]->description = number_format($price_kids_c,2,',','.').' € '.$products->entry_list[$key]->description;
            }

            $shipUid = 0;
            $shipUid = $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->uid;
            $ship = $this->shipRepository->findByUid($shipUid);
            if ($ship) {
                $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->hulk_lenght = floatval($ship->getLength());
                //$this->crmCall('set_entry', 'HS_Schiffe', $this->crmConnector->formatNameValueList(array('id' => $products->entry_list[$key]->id, 'hulk_lenght' => $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->hulk_lenght)));
            }

            if ($ship && $products->entry_list[$key]->liegeplatz_c == '') {
                $products->entry_list[$key]->liegeplatz_c = $ship->getBerth();
                //$this->crmCall('set_entry', 'AOS_Products', $this->crmConnector->formatNameValueList(array('id' => $products->entry_list[$key]->id, 'liegeplatz_c' => $products->entry_list[$key]->liegeplatz_c)));

            }

            if ($products->entry_list[$key]->inkludierte_leistungen_c == '' && $ship) {
                $daytrips = $this->daytripRepository->findByTitle($ship->getName());
                foreach($daytrips as $daytrip) {
                    if (
                        $daytrip->getDate()->format('Y-m-d') == $products->entry_list[$key]->datum_c &&
                        $daytrip->getTime() == $products->entry_list[$key]->zeit_c
                    ) {
                        $products->entry_list[$key]->inkludierte_leistungen_c = $daytrip->getComment();
                        //$this->crmCall('set_entry', 'AOS_Products', $this->crmConnector->formatNameValueList(array('id' => $products->entry_list[$key]->id, 'inkludierte_leistungen_c' => $products->entry_list[$key]->inkludierte_leistungen_c)));

                    }
                }
            }

            $country = $this->findCountry($products->entry_list[$key]->hs_schiffe_aos_products_1[0]->flagge);
            if ($country) {
                $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->isoA2Code = strtolower($country->getIsoCodeA2());
                $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->country = $country->getNameLocalized();

            } else {
                $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->isoA2Code = false;
                $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->country = false;
            }

            if ($ship) {
                if ($ship->getImage()) {
                    // TODO: Shrink imagesize
                    $products->entry_list[$key]->hs_schiffe_aos_products_1[0]->image = $ship->getImage()->getOriginalResource()->getPublicUrl();
                }
            }

            $products->entry_list[$key]->bookingurl = $this->controllerContext->
            getUriBuilder()->reset()->
            uriFor('form', array('id' => $product->id));

            if ($products->entry_list[$key]->type == 'Deaktiviert' || $products->entry_list[$key]->type == 'deaktiviert') {
                unset($products->entry_list[$key]);
            }
        }
        return $products;
    }

    /**
     * Try to find the static_info_table_record.
     * @param $search
     * @return object
     */
    protected function findCountry($search) {
        if (strlen($search) == 3) {
            $result = $this->countryRepository->findByIsoCodeA3(strtolower($search));
            if (count($result)) {
                return $result[0];
            }
        }
        $result = $this->countryRepository->findByOfficialNameEn(strtolower($search));
        if (count($result)) {
            return $result[0];
        }
        $result = $this->countryRepository->findByOfficialNameLocal(strtolower($search));
        if (count($result)) {
            return $result[0];
        }
        $result = $this->countryRepository->findByShortNameEn(strtolower($search));
        if (count($result)) {
            return $result[0];
        }
        $result = $this->countryRepository->findByShortNameLocal(strtolower($search));
        if (count($result)) {
            return $result[0];
        }
        $result = $this->countryRepository->findByTopLevelDomain(strtolower($search));
        if (count($result)) {
            return $result[0];
        }
        return false;
    }

    /**
     * Is shown when a communication with the crm is not possible
     */
    public function crmErrorAction()
    {

    }

    /**
     * Generates the form and generates the quote for the crm
     *
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function formAction()
    {
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/FormPriceCalc.js', 'text/javascript');

        $product = false;
        $finalQuote = false;
        if ($this->request->hasArgument('id')) {
            $product = $this->crmCall(
                'get_entry',
                'AOS_Products',
                $this->request->getArgument('id'),
                $this->productFields,               // Select Fields
                array(
                    array(
                        'name' => 'hs_schiffe_aos_products_1',
                        'value' => $this->shipFields,
                    ),
                ));
            $product = $this->crmConnector->removeOverhead($product);

            $shipId = '';
            if (isset($product->entry_list[0]->hs_schiffe_aos_products_1[0]->id)) {
                $shipId = $product->entry_list[0]->hs_schiffe_aos_products_1[0]->id;
            }

        } else {
            $this->redirect('list');
        }

        //$this->view->assign('isJakota', ($_SERVER['REMOTE_ADDR'] == '80.153.50.34'));
        $this->view->assign('isJakota', true);

        $section = 'form';
        if ($this->request->hasArgument('save')) {
            $section = 'list';
            // Create Contact
            $contact = $this->createContact();
            if (isset($contact->body->id)) {
                // Create Quote
                $quote = $this->createQuote($contact);
                if (isset($product->entry_list[0]->id) && isset($quote->body->id)) {
                    if ($this->request->getArgument('extendexBooking') == 0) {
                        $booking = $this->request->getArgument('booking');
                        // Create Line Items
                        // Product
                        $erwachsene = $booking['paticipants'];
                        $kinder = $booking['children'];


                        $lineItem1 = $this->createLineItem(array(
                            'name' => $product->entry_list[0]->name,
                            'item_description' => 'Erwachsene',
                            'description' => $booking['notes'],
                            'product_qty' => $booking['paticipants'],
                            'product_list_price' => floatval($product->entry_list[0]->price),
                            'product_discount' => 0,
                            'product_unit_price' => floatval($product->entry_list[0]->price),
                            'product_total_price' => floatval($product->entry_list[0]->price * $booking['paticipants']),
                            'parent_type' => 'AOS_Quotes',
                            'vat' => 0,
                            'parent_id' => $quote->body->id,
                            'product_id' => $product->entry_list[0]->id,
                        ));
                        if ($booking['children'] > 0) {

                            // Use price_kids_c if available
                            $price = floatval($product->entry_list[0]->price);
                            if (floatval($product->entry_list[0]->price_kids_c) > 0) {
                                $price = floatval($product->entry_list[0]->price_kids_c);
                            }

                            $lineItem1 = $this->createLineItem(array(
                                'name' => $product->entry_list[0]->name,
                                'item_description' => 'Kinder',
                                'description' => $booking['notes'],
                                'product_qty' => $booking['children'],
                                'product_list_price' => $price,
                                'product_discount' => 0,
                                'product_unit_price' => $price,
                                'product_total_price' => floatval($price * $booking['children']),
                                'parent_type' => 'AOS_Quotes',
                                'vat' => 0,
                                'parent_id' => $quote->body->id,
                                'product_id' => $product->entry_list[0]->id,
                            ));
                        }

                        $bookingType = $this->getBookingType($booking['type']);

                        // Service
                        if ($product->entry_list[0]->id != '15d3662f-653f-6ff5-fc67-5ee361bb74dc') {
                            $lineItem2 = $this->createLineItem(array(
                                'name' => $bookingType->name,
                                'product_list_price' => $bookingType->price,
                                'product_discount' => 0,
                                'product_unit_price' => $bookingType->price,
                                'product_total_price' => $bookingType->price,
                                'parent_type' => 'AOS_Quotes',
                                'vat' => 0,
                                'parent_id' => $quote->body->id,
                            ));
                        }

                        $this->crmCall('set_entry', 'AOS_Quotes', $this->crmConnector->formatNameValueList(array('id' => $quote->body->id, 'versandart_c' => $bookingType->shipment, 'zahlungsmethode_c' => $bookingType->payment, 'hs_schiffe_aos_quotes_1hs_schiffe_ida' => $shipId)));
                        $this->crmCall(
                            'set_relationship',
                            'AOS_Quotes',
                            $quote->body->id,
                            'hs_schiffe_aos_quotes_1hs_schiffe_ida',
                            array($shipId)
                        );

                        $this->crmCall('set_relationship', 'AOS_Quotes', $this->crmConnector->formatNameValueList(array('id' => $quote->body->id, 'versandart_c' => $bookingType->shipment, 'zahlungsmethode_c' => $bookingType->payment, 'hs_schiffe_aos_quotes_1hs_schiffe_ida' => $shipId)));
                        $finalQuote = $this->crmCall('get_entry', 'AOS_Quotes', $quote->body->id);
                    } else {
                        $booking = $this->request->getArgument('booking');
                        foreach($booking['participants'] as $participant) {
                            if ($participant['ischild'] == 1) {

                                // Use price_kids_c if available
                                $price = floatval($product->entry_list[0]->price);
                                if (floatval($product->entry_list[0]->price_kids_c) > 0) {
                                    $price = floatval($product->entry_list[0]->price_kids_c);
                                }

                                $description = ($participant['salutation'] == 1 ? 'Frau' : 'Herr').' '.$participant['first_name'].' '.$participant['last_name']."\nKIND";
                            } else {

                                $price = floatval($product->entry_list[0]->price);

                                $description = ($participant['salutation'] == 1 ? 'Frau' : 'Herr').' '.$participant['first_name'].' '.$participant['last_name']."\nPA-Nr.: ".$participant['panr'];
                            }

                            $lineItem1 = $this->createLineItem(array(
                                'name' => $product->entry_list[0]->name,
                                'item_description' => $description,
                                'description' => $booking['notes'],
                                'product_qty' => 1,
                                'product_list_price' => $price,
                                'product_discount' => 0,
                                'product_unit_price' => $price,
                                'product_total_price' => floatval($product->entry_list[0]->price),
                                'parent_type' => 'AOS_Quotes',
                                'vat' => 0,
                                'parent_id' => $quote->body->id,
                                'product_id' => $product->entry_list[0]->id,
                            ));
                        }

                        $bookingType = $this->getBookingType($booking['type']);

                        // Service
                        if ($product->entry_list[0]->id != '15d3662f-653f-6ff5-fc67-5ee361bb74dc') {
                            $lineItem2 = $this->createLineItem(array(
                                'name' => $bookingType->name,
                                'product_list_price' => $bookingType->price,
                                'product_discount' => 0,
                                'product_unit_price' => $bookingType->price,
                                'product_total_price' => $bookingType->price,
                                'parent_type' => 'AOS_Quotes',
                                'vat' => 0,
                                'parent_id' => $quote->body->id,
                            ));
                        }

                        $this->crmCall('set_entry', 'AOS_Quotes', $this->crmConnector->formatNameValueList(array('id' => $quote->body->id, 'versandart_c' => $bookingType->shipment, 'zahlungsmethode_c' => $bookingType->payment, 'hs_schiffe_aos_quotes_1hs_schiffe_ida' => $shipId)));
                        $this->crmCall(
                            'set_relationship',
                            'AOS_Quotes',
                            $quote->body->id,
                            'hs_schiffe_aos_quotes_1hs_schiffe_ida',
                            array($shipId)
                        );
                        $finalQuote = $this->crmCall('get_entry', 'AOS_Quotes', $quote->body->id);

                    }

                    if ($finalQuote) {
                        switch($bookingType->paymentService) {
                            case 'sofort':
                                if ($this->request->hasArgument('total')) {
                                    $Sofortueberweisung = new \Sofort\SofortLib\Sofortueberweisung($this->sofortConfigKey);
                                    $Sofortueberweisung->setAmount(floatval($this->request->getArgument('total')));
                                    $Sofortueberweisung->setCurrencyCode('EUR');
                                    $Sofortueberweisung->setReason(utf8_encode($product->entry_list[0]->name), utf8_encode($finalQuote->body->entry_list[0]->name_value_list->name->value));
                                    $Sofortueberweisung->setSuccessUrl($this->controllerContext->getUriBuilder()
                                        ->reset()
                                        ->setCreateAbsoluteUri(true)
                                        ->uriFor('sofortOk', array('id' => $quote->body->id)), true);
                                    $Sofortueberweisung->setAbortUrl($this->controllerContext->getUriBuilder()
                                        ->reset()
                                        ->setCreateAbsoluteUri(true)
                                        ->uriFor('sofortAbort', array('id' => $quote->body->id)));
                                    /*$Sofortueberweisung->setNotificationUrl($this->controllerContext->getUriBuilder()
                                        ->reset()
                                        ->setCreateAbsoluteUri(true)
                                        ->setTargetPageUid($GLOBALS['TSFE']->id)
                                        ->setNoCache(true)
                                        ->setArguments(array('eID' => 'hansesailBookingSofortNotification'))
                                        ->build());*/
                                    $Sofortueberweisung->sendRequest();
                                    if($Sofortueberweisung->isError()) {
                                        echo $Sofortueberweisung->getError();
                                    } else {
                                        //get unique transaction ID useful for check payment status and put in on the quote
                                        $transactionId = $Sofortueberweisung->getTransactionId();
                                        $this->crmCall('set_entry', 'AOS_Quotes', $this->crmConnector->formatNameValueList(array('id' => $quote->body->id, 'sofort_transaction_id_c' => $transactionId)));
                                        $this->sofortLog($quote->body->id, 'Transaktion begonnen');
                                        $this->sofortLog($quote->body->id, 'Transaktion ID ist: '.$transactionId);
                                        //buyer must be redirected to $paymentUrl else payment cannot be successfully completed!
                                        $paymentUrl = $Sofortueberweisung->getPaymentUrl();
                                        header('Location: '.$paymentUrl); exit;
                                    }
                                }
                                break;
                            case 'CreditCard':
                                $okURL = $this->controllerContext->getUriBuilder()->reset()->setCreateAbsoluteUri(true)->uriFor('sofortOk', array('id' => $quote->body->id));
                                $errorURL = $this->controllerContext->getUriBuilder()->reset()->setCreateAbsoluteUri(true)->uriFor('sofortAbort', array('id' => $quote->body->id));
                                $apiURL = 'https://wpp.wirecard.com/api/payment/register';
                                $apiUser = 'HanseSail-7789143';
                                $apiPassword = '1f014g6Sg7H3-P';
                                $accountID = '5ccb900c-f46d-4a2f-983b-d7710c30bb56';
                                $accountSecret = '1e96a97c-842a-41c2-a092-9eb9d0120f11';
                                $jsonCall = '{
  "payment": {
    "merchant-account-id": {
      "value": "'.$accountID.'"
    },
    "account-holder": {
      "first-name": "",
      "last-name": ""
    },
    "request-id": "web-'.$quote->body->id.'",
    "requested-amount": {
      "value": '.floatval($this->request->getArgument('total')).',
      "currency": "EUR"
    },
    "descriptor": "'.utf8_encode($product->entry_list[0]->name).' - '.date('d.m.Y H:i:s').'",
    "transaction-type": "purchase",
    "payment-methods": {
      "payment-method": [
        {
          "name": "creditcard"
        }
      ]
    },
    "three-d": {
      "attempt-three-d": "true"
    },
    "notifications": {
      "format": "application/xml",
      "notification": []
    },
    "success-redirect-url": "'.$okURL.'",
    "fail-redirect-url": "'.$errorURL.'",
    "cancel-redirect-url": "'.$errorURL.'"
  }
}';
                                $ch = curl_init($apiURL);
                                curl_setopt($ch, CURLOPT_URL, $apiURL);
                                curl_setopt($ch, CURLOPT_USERPWD, $apiUser . ":" . $apiPassword);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonCall);
                                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                                curl_setopt($ch, CURLOPT_HEADER, 1);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                                $response = curl_exec($ch);
                                $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                                $header = substr($response, 0, $header_size);
                                $body = substr($response, $header_size);
                                $return = json_decode($body, true);
                                if (!array_key_exists('errors', $return)) {
                                    header('Location: '.$return['payment-redirect-url']); exit;
                                } else {
                                    header('Location: '.$errorURL); exit;
                                }
                                break;
                        }
                    }



                    $section = 'ok';
                    $this->redirect('list', 'Booking', 'HansesailBooking', array('section' => 'formSubmitted'));
                } else {
                    // Fallback could not create Quote - should delete contact
                    $this->removeContact($contact);
                    $section = 'error2';
                }
            } else {
                $section = 'error1';
            }
        }

        if (isset($product->entry_list[0]->id)) {
            $product = $this->augumentProducts($product);
            $this->view->assign('extendexBooking', $this->extendedBookingNeeded($product));
            $this->view->assign('product', $product);
        } else {
            $this->redirect('list');
        }

        $this->view->assign('section', $section);
    }

    private function createContact()
    {
        try {
            $contact = $this->request->getArgument('contact');
        } catch (\Exeption $e) {
            $contact = array();
            return false;
        }
        $this->view->assign('contact', $contact);
        $query = 'contacts.first_name = "'.$contact['first_name'].'" AND contacts.last_name = "'.$contact['last_name'].'" AND contacts.primary_address_street = "'.$contact['primary_address_street'].'" AND contacts.primary_address_postalcode = "'.$contact['primary_address_postalcode'].'"';
        $result = $this->crmConnector->get_entry_list('Contacts', $query);
        if($result->body->result_count >= 1) {
            //to update a record, you will nee to pass in a record id
            $contact['id'] = $result->body->entry_list[0]->id;
            return $this->crmCall('set_entry', 'Contacts', $this->crmConnector->formatNameValueList($contact));
        } else {
           return $this->crmCall('set_entry', 'Contacts', $this->crmConnector->formatNameValueList($contact));
        }
    }

    /*
    * TODO: Return if extended Booking (Name and ID of all Participants) is needed
    */

    private function createQuote($contact)
    {
        $quote = array(
            'expiration' => date('Y-m-d', mktime(0, 0, 0, date('m'), (date('d') + 14), date('Y'))),
            'stage' => 'Web',
            'invoice_status' => 'Not Invoiced',
            'assigned_user_id' => $this->assignedCRMUser,
            'billing_contact_id' => $contact->body->id,
            'billing_address_street' => $contact->body->entry_list->primary_address_street->value,
            'billing_address_city' => $contact->body->entry_list->primary_address_city->value,
            'billing_address_country' => $contact->body->entry_list->primary_address_country->value,
            'billing_address_postalcode' => $contact->body->entry_list->primary_address_postalcode->value,
        );
        return $this->crmCall('set_entry', 'AOS_Quotes', $this->crmConnector->formatNameValueList($quote));
    }

    private function createLineItem($lineItem)
    {
        return $this->crmCall('set_entry', 'AOS_Products_Quotes', $this->crmConnector->formatNameValueList($lineItem));
    }

    /**
     * Returns Typename, Price, Shipment, and PaymentService for a Booking Type
     * @param $type Bookingtype from the form
     * @return \stdClass
     */
    private function getBookingType($type) {
        $bookinType = new \stdClass();
        switch($type) {
            case 1:
                $bookinType->price = 4.5;
                $bookinType->name = 'Barzahlung vor Ort, Tickets und Rechnung bei Abholung';
                $bookinType->shipment = 'Abholung'; // Post, EMail, Abholung
                $bookinType->payment = 'Bar';
                $bookinType->paymentService = false;
                break;
            case 2:
                $bookinType->price = 4.5;
                $bookinType->name = 'Servicegebühr';
                $bookinType->shipment = 'Abholung'; // Post, EMail, Abholung
                $bookinType->payment = 'EC';
                $bookinType->paymentService = false;
                break;
            case 3:
                $bookinType->price = 4;
                $bookinType->name = 'Servicegebühr';
                $bookinType->shipment = 'EMail'; // Post, EMail, Abholung
                $bookinType->payment = 'Sofort';
                $bookinType->paymentService = 'sofort';
                break;
            case 4:
                $bookinType->price = 4;
                $bookinType->name = 'Servicegebühr';
                $bookinType->shipment = 'Post'; // Post, EMail, Abholung
                $bookinType->payment = 'Sofort';
                $bookinType->paymentService = 'sofort';
                break;
            case 5:
                $bookinType->price = 4;
                $bookinType->name = 'Servicegebühr';
                $bookinType->shipment = 'Post'; // Post, EMail, Abholung
                $bookinType->payment = 'Rechnung';
                $bookinType->paymentService = false;
                break;
            case 8:
                $bookinType->price = 4;
                $bookinType->name = 'Servicegebühr';
                $bookinType->shipment = 'EMail'; // Post, EMail, Abholung
                $bookinType->payment = 'CreditCard';
                $bookinType->paymentService = 'CreditCard';
                break;
            case 6: default:
                $bookinType->price = 4;
                $bookinType->name = 'Servicegebühr';
                $bookinType->shipment = 'EMail'; // Post, EMail, Abholung
                $bookinType->payment = 'Rechnung';
                $bookinType->paymentService = false;
                break;

        }
        return $bookinType;
    }

    private function removeContact($contact)
    {
        return $this->crmCall('set_entry', 'Contacts', $this->crmConnector->formatNameValueList(array('id' => $contact->body->id, 'deleted' => '1')));
    }

    private function extendedBookingNeeded($product) {
        return false;
    }

    /**
     * Initializes the controller before invoking an action method.
     * Solve tasks which all actions have in common.
     *
     * @return void
     * @api
     */
    protected function initializeAction() {
        $this->pageRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);
        
        $this->pageRenderer->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/css/bootstrap-datepicker3.min.css');
        $this->pageRenderer->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/css/flag-icon.min.css');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/underscore-min.js', 'text/javascript');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/backbone-min.js', 'text/javascript');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/moment.min.js', 'text/javascript');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/bootstrap-datepicker.min.js', 'text/javascript');
        $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/locales/bootstrap-datepicker.de.min.js', 'text/javascript');
        if (is_file(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()).'Resources/Public/css/General.css')) {
            $this->pageRenderer->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/css/General.css');
        }
        if (is_file(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/General.js')) {
            $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/General.js', 'text/javascript');
        }
    }

    /**
     * Initializes the view before invoking an action method.

     * @param ViewInterface $view
     *
     * @return void
     * @api
     */
    protected function initializeView(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface $view) {
        $actionName = ucfirst($this->request->getControllerActionName());

        switch($actionName) {
            case 'Form':
                $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/jquery.validate.js', 'text/javascript');
                $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/locales/messages_de.js', 'text/javascript');
                $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/vendor/additional-methods.js', 'text/javascript');
                break;
        }

        if (is_file(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()).'Resources/Public/css/'.$actionName.'.css')) {
            $this->pageRenderer->addCssFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/css/'.$actionName.'.css');
        }
        if (is_file(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/'.$actionName.'.js')) {
            $this->pageRenderer->addJsFooterFile(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::siteRelPath($this->request->getControllerExtensionKey()).'Resources/Public/JavaScript/'.$actionName.'.js', 'text/javascript');
        }
    }
}