<?php
/**
 * Created by PhpStorm.
 * User: webmaster
 * Date: 01.06.16
 * Time: 15:02
 */

$libraryClassesPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hansesail_booking') . 'Resources/Private/PHP/';
return array(
    'Sofort\SofortLib\AbstractDataHandler'      => $libraryClassesPath . 'Sofort/SofortLib/AbstractDataHandler.php',
    'Sofort\SofortLib\AbstractHttp'             => $libraryClassesPath . 'Sofort/SofortLib/AbstractHttp.php',
    'Sofort\SofortLib\AbstractLoggerHandler'    => $libraryClassesPath . 'Sofort/SofortLib/AbstractLoggerHandler.php',
    'Sofort\SofortLib\AbstractWrapper'          => $libraryClassesPath . 'Sofort/SofortLib/AbstractWrapper.php',
    'Sofort\SofortLib\Billcode'                 => $libraryClassesPath . 'Sofort/SofortLib/Billcode.php',
    'Sofort\SofortLib\BillcodeDetails'          => $libraryClassesPath . 'Sofort/SofortLib/BillcodeDetails.php',
    'Sofort\SofortLib\Factory'                  => $libraryClassesPath . 'Sofort/SofortLib/Factory.php',
    'Sofort\SofortLib\FileLogger'               => $libraryClassesPath . 'Sofort/SofortLib/FileLogger.php',
    'Sofort\SofortLib\HttpCurl'                 => $libraryClassesPath . 'Sofort/SofortLib/HttpCurl.php',
    'Sofort\SofortLib\HttpSocket'               => $libraryClassesPath . 'Sofort/SofortLib/HttpSocket.php',
    'Sofort\SofortLib\Ideal'                    => $libraryClassesPath . 'Sofort/SofortLib/Ideal.php',
    'Sofort\SofortLib\IdealBanks'               => $libraryClassesPath . 'Sofort/SofortLib/IdealBanks.php',
    'Sofort\SofortLib\IdealNotification'        => $libraryClassesPath . 'Sofort/SofortLib/IdealNotification.php',
    'Sofort\SofortLib\Multipay'                 => $libraryClassesPath . 'Sofort/SofortLib/Multipay.php',
    'Sofort\SofortLib\Notification'             => $libraryClassesPath . 'Sofort/SofortLib/Notification.php',
    'Sofort\SofortLib\Paycode'                  => $libraryClassesPath . 'Sofort/SofortLib/Paycode.php',
    'Sofort\SofortLib\PaycodeAbstract'          => $libraryClassesPath . 'Sofort/SofortLib/PaycodeAbstract.php',
    'Sofort\SofortLib\PaycodeDetails'           => $libraryClassesPath . 'Sofort/SofortLib/PaycodeDetails.php',
    'Sofort\SofortLib\PaycodeDetailsAbstract'   => $libraryClassesPath . 'Sofort/SofortLib/PaycodeDetailsAbstract.php',
    'Sofort\SofortLib\Refund'                   => $libraryClassesPath . 'Sofort/SofortLib/Refund.php',
    'Sofort\SofortLib\Sofortueberweisung'       => $libraryClassesPath . 'Sofort/SofortLib/Sofortueberweisung.php',
    'Sofort\SofortLib\TransactionData'          => $libraryClassesPath . 'Sofort/SofortLib/TransactionData.php',
    'Sofort\SofortLib\XmlDataHandler'           => $libraryClassesPath . 'Sofort/SofortLib/XmlDataHandler.php',
    'Sofort\SofortLib\Xml\ArrayToXml'           => $libraryClassesPath . 'Sofort/SofortLib/Xml/ArrayToXml.php',
    'Sofort\SofortLib\Xml\ArrayToXmlException'  => $libraryClassesPath . 'Sofort/SofortLib/Xml/ArrayToXmlException.php',
    'Sofort\SofortLib\Xml\XmlToArray'           => $libraryClassesPath . 'Sofort/SofortLib/Xml/XmlToArray.php',
    'Sofort\SofortLib\Xml\XmlToArrayException'  => $libraryClassesPath . 'Sofort/SofortLib/Xml/XmlToArrayException.php',
    'Sofort\SofortLib\Xml\XmlToArrayNode'       => $libraryClassesPath . 'Sofort/SofortLib/Xml/XmlToArrayNode.php',
    'Sofort\SofortLib\Xml\Element\Element'      => $libraryClassesPath . 'Sofort/SofortLib/Xml/Element/Element.php',
    'Sofort\SofortLib\Xml\Element\Tag'          => $libraryClassesPath . 'Sofort/SofortLib/Xml/Element/Tag.php',
    'Sofort\SofortLib\Xml\Element\Text'         => $libraryClassesPath . 'Sofort/SofortLib/Xml/Element/Text.php',
);