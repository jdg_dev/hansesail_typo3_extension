<?php
namespace JAKOTA\HansesailBooking\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Fünning <fuenning@jakota.de>, JAKOTA Design Group GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\FormProtection\Exception;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

require_once 'phar://' . ExtensionManagementUtility::extPath('hansesail_booking') . 'Classes/Utility/httpful.phar/Httpful/Bootstrap.php';
use Httpful\Request;

/**
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SuiteCrmConnector
{

    protected $apiUsername = 'web';
    protected $apiPassword = 'asdf123**';
    protected $apiApplication = 'HanseSail';
    protected $apiURL = 'https://crm.hansesail.com/suite/service/v4_1/rest.php';
    //protected $apiURL = 'http://91.250.115.208/suitecrm/service/v4_1/rest.php';
    protected $timeout = 15;
    protected $apiUser = false;

    /**
     * @var $log \TYPO3\CMS\Core\Log\LogManager
     */
    protected $log;

    public function __construct()
    {

        // INIT the TYPO§ Logger Logs can be found in /typo3temp/logs/typo3.log
        $this->log = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);

        // Try to login
        $response = $this->post('login', array(
            'user_auth' => array(
                'user_name' => $this->apiUsername,
                'password' => md5($this->apiPassword),
                'application' => $this->apiApplication,
            )
        ));
        if ($response) {
            if (isset($response->body->id)) {
                $this->apiUser = $response->body;
            } else if (isset($response->body->name)) {
                $this->log->error('Login attempt failed: ' . $response->body->name . ' - ' . $response->body->description);
            } else {
                $this->log->error('Login attempt failed: unknown reason');
            }
        } else {
            $this->log->error('Login attempt failed: no server response');
        }
    }

    protected function post($method, $data)
    {
        return $this->request('post', $method, $data);
    }

    protected function request($httpMethod, $method, $data)
    {
        try {
            $result = Request::$httpMethod($this->apiURL)
                ->body(array(
                    'method' => $method,
                    'input_type' => 'JSON',
                    'response_type' => 'JSON',
                    'rest_data' => json_encode($data)
                ), 'multipart/form-data')
                ->timeout($this->timeout)
                ->expectsJson()
                ->send();
            return $result;
        } catch (\Exception $e) {
            $this->log->error($e->getMessage());
            return false;
        }
    }

    /**
     * @param $module_name The name of the module from which to retrieve records. Note: This is the modules key which may not be the same as the modules display name.
     * @param string $query The SQL WHERE clause without the word "where". You should remember to specify the table name for the fields to avoid any ambiguous column errors.
     * @param string $order_by The SQL ORDER BY clause without the phrase "order by".
     * @param int $offset The record offset from which to start.
     * @param array $select_fields The list of fields to be returned in the results. Specifying an empty array will return all fields.
     * @param array $link_name_to_fields_array A list of link names and the fields to be returned for each link.
     * @param int $max_results The maximum number of results to return.
     * @param int $deleted If deleted records should be included in the results.
     * @param bool|false $favorites If only records marked as favorites should be returned.
     * @return object API Response
     * @throws \Exception
     */
    public function get_entry_list($module_name, $query = "", $order_by = "", $offset = 0, $select_fields = array(), $link_name_to_fields_array = array(), $max_results = 999999, $deleted = 0, $favorites = false)
    {
        if ($this->apiUser) {
            $response = $this->post('get_entry_list', array(
                'session' => $this->apiUser->id,
                'module_name' => $module_name,
                'query' => $query,
                'order_by' => $order_by,
                'offset' => $offset,
                'select_fields' => $select_fields,
                'link_name_to_fields_array' => $link_name_to_fields_array,
                'max_results' => $max_results,
                'deleted' => $deleted,
                'favorites' => $favorites
            ));
            return $response;
        } else {
            throw new \Exception('SuiteCrmConnector: No session data available to run the request.');
        }
    }

    /**
     * @param $module_name The name of the module from which to retrieve records. Note: This is the modules key which may not be the same as the modules display name.
     * @param $id The ID of the record to retrieve.
     * @param array $select_fields The list of fields to be returned in the results. Specifying an empty array will return all fields.
     * @param array $link_name_to_fields_array A list of link names and the fields to be returned for each link.
     * @param bool|false $track_view Flag the record as a recently viewed item.
     * @return object API Response
     * @throws \Exception
     */
    public function get_entry($module_name, $id, $select_fields = array(), $link_name_to_fields_array = array(), $track_view = false)
    {
        if ($this->apiUser) {
            $response = $this->post('get_entry', array(
                'session' => $this->apiUser->id,
                'module_name' => $module_name,
                'id' => $id,
                'select_fields' => $select_fields,
                'link_name_to_fields_array' => $link_name_to_fields_array,
                'track_view' => $track_view
            ));
            return $response;
        } else {
            throw new \Exception('SuiteCrmConnector: No session data available to run the request.');
        }
    }

    /**
     * @param $module_name The name of the module from which to retrieve records. Note: This is the modules key which may not be the same as the modules display name.
     * @param array $name_value_list The name/value list of the record attributes.
     * @return bool
     * @throws \Exception
     */
    public function set_entry($module_name, $name_value_list = array())
    {
        if ($this->apiUser) {
            $response = $this->post('set_entry', array(
                'session' => $this->apiUser->id,
                'module_name' => $module_name,
                'name_value_list' => $name_value_list
            ));
            return $response;
        } else {
            throw new \Exception('SuiteCrmConnector: No session data available to run the request.');
        }
    }

    /**
     * @param $module_name The name of the module from which to retrieve records. Note: This is the modules key which may not be the same as the modules display name.
     * @param $module_id The ID of the specified module record.
     * @param $link_field_name The name of the link field for the related module.
     * @param $related_ids The list of related record IDs you are relating
     * @param array $name_value_list An array specifying relationship fields to populate. An example of this is contact_role between Opportunities and Contacts.
     * @param int $delete Determines whether the relationship is being created or deleted. 0:create, 1:delete
     * @return bool
     * @throws \Exception
     */
    public function set_relationship($module_name = null, $module_id = null, $link_field_name = null, $related_ids = null, $name_value_list = array(), $delete = 0)
    {
        if ($this->apiUser) {
            $response = $this->post('set_relationship', array(
                'session' => $this->apiUser->id,
                'module_name' => $module_name,
                'module_id' => $module_id,
                'link_field_name' => $link_field_name,
                'related_ids' => $related_ids,
                'name_value_list' => $name_value_list,
                'delete' => $delete,
            ));
            return $response;
        } else {
            throw new \Exception('SuiteCrmConnector: No session data available to run the request.');
        }
    }

    /**
     * Formats a PHP associative array to a crm compatible name-value list.
     *
     * @param $source
     * @return array
     */
    public function formatNameValueList($source)
    {
        $nameValueList = array();
        foreach ($source as $name => $value) {
            switch ($name) {
                case 'salutation':
                    $salutionMap = array(
                        0 => 'Mr.',
                        1 => 'Ms.',
                    );
                    $nameValue = array(
                        'name' => $name,
                        'value' => $salutionMap[$value],
                    );
                    break;
                default:
                    $nameValue = array(
                        'name' => $name,
                        'value' => $value,
                    );
                    break;
            }
            $nameValueList[] = $nameValue;
        }
        return $nameValueList;
    }

    /**
     * Reduces the Overhead of a api response
     *
     * @param array $apiResponse
     * @uses \JAKOTA\HansesailBooking\Utility\SuiteCrmConnector::shrink_get_entry_list() on get_entry_list methode.
     * @uses \JAKOTA\HansesailBooking\Utility\SuiteCrmConnector::shrink_get_entry() on get_entry methode.
     * @return array
     */
    public function removeOverhead($apiResponse)
    {
        $method = 'shrink_' . $apiResponse->request->payload['method'];
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), array($apiResponse));
        }
        return $apiResponse;
    }

    protected function get($method, $data)
    {
        return $this->request('get', $method, $data);
    }

    protected function put($method, $data)
    {
        return $this->request('put', $method, $data);
    }

    /**
     * Shrinks the Resonse Overhead from the CRM methode get_entry_list.
     * is used by removeOverhead
     * @param $apiResponse
     * @return \stdClass
     */
    private function shrink_get_entry_list($apiResponse)
    {
        $processedResponse = new \stdClass();
        $processedResponse->result_count = $apiResponse->body->result_count;
        $processedResponse->total_count = $apiResponse->body->total_count;
        $processedResponse->next_offset = $apiResponse->body->next_offset;
        $processedResponse->entry_list = array();
        foreach ($apiResponse->body->entry_list as $entryNo => $entry) {
            $newEntry = new \stdClass();
            foreach ($entry->name_value_list as $key => $nameValueItem) {
                $newEntry->$key = $nameValueItem->value;
            }
            foreach ($apiResponse->body->relationship_list[$entryNo]->link_list as $link) {
                $linkKey = $link->name;
                $records = array();
                foreach ($link->records as $record) {
                    $newRecord = new \stdClass();
                    foreach ($record->link_value as $key => $nameValueItem) {
                        $newRecord->$key = $nameValueItem->value;
                    }
                    $records[] = $newRecord;
                }
                $newEntry->$linkKey = $records;
            }
            $processedResponse->entry_list[] = $newEntry;
        }
        return $processedResponse;
    }

    /**
     * Shrinks the Resonse Overhead from the CRM methode get_entry.
     * is used by removeOverhead
     * @param $apiResponse
     * @return \stdClass
     */
    private function shrink_get_entry($apiResponse)
    {
        $processedResponse = new \stdClass();
        $processedResponse->result_count = count($apiResponse->body->entry_list);
        $processedResponse->total_count = count($apiResponse->body->entry_list);
        $processedResponse->next_offset = 0;
        $processedResponse->entry_list = array();
        foreach ($apiResponse->body->entry_list as $entryNo => $entry) {
            $newEntry = new \stdClass();
            foreach ($entry->name_value_list as $key => $nameValueItem) {
                $newEntry->$key = $nameValueItem->value;
            }
            foreach ($apiResponse->body->relationship_list[$entryNo] as $link) {
                $linkKey = $link->name;
                $records = array();
                foreach ($link->records as $record) {
                    $newRecord = new \stdClass();
                    foreach ($record as $key => $nameValueItem) {
                        $newRecord->$key = $nameValueItem->value;
                    }
                    $records[] = $newRecord;
                }
                $newEntry->$linkKey = $records;
            }
            $processedResponse->entry_list[] = $newEntry;
        }
        return $processedResponse;
    }
}