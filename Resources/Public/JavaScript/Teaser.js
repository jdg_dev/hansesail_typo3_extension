/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 24.03.16
 * Time: 14:00
 */
function r(f){(/in/.test(document.readyState) || typeof $ != 'function' || typeof _ != 'function')?setTimeout(r,9,f):f()}
r(function(){
    var selects = $('.hansesailBookingTeaserSelectorcontainer select');

    selects.each(function() {
        $(this).val('');
    });
    selects.change(function() {
        var $this = $(this);
        selects.prop('disabled', true);
        $this.prop('disabled', false);
        $('#hansesailBookingTeaserButton').text('Seite wird geladen').addClass('disabled');
        window.location.href = $this.data('target')+'#'+$this.val()
                .replace(' ', '[_]')
                .replace('Ä', '[AE]')
                .replace('Ö', '[OE]')
                .replace('Ü', '[UE]')
                .replace('ä', '[ae]')
                .replace('ö', '[oe]')
                .replace('ü', '[ue]')
                .replace('ß', '[ss]');
    });
});