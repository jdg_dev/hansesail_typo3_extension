/**
 * User: Martin Fünning (JAKOTA Design Group GmbH)
 * Date: 17.02.16
 * Time: 09:33
 */
function r(f){(/in/.test(document.readyState) || typeof $ != 'function' || typeof _ != 'function')?setTimeout(r,9,f):f()}
r(function(){

    if ($('#price_template').length) {
        var $priceTemplate = _.template($('#price_template').html());
        var priceCalculator = new FormPriceCalc();
        var priceOverviewTarget = $('#price_overview');
        window.setInterval(function() {
            var priceObject = priceCalculator.getPriceObject();
            priceOverviewTarget.empty().append($priceTemplate({priceObject: priceObject}));
        }, 200);
    }

    if ($('#teilnehmerTemplate').length) {
        _.templateSettings.interpolate = /\{\{(.+?)\}\}/g;
        var teilnehmerTemplate = _.template($('#teilnehmerTemplate').html());

        var $teilnehmerCount = $('#teilnehmerCount');
        var $teilnehmerContainer = $('#teilnehmerContainer');
        $teilnehmerCount.change(function() {
            setTeilnehmerCount($(this).val());
        });
        setTeilnehmerCount($teilnehmerCount.val());

        function setTeilnehmerCount(number) {
            var c = 1;
            $teilnehmerContainer.find('.teilnehmerRow').each(function() {
                if ($(this).data('number') > number) {
                    $(this).slideUp('fast', _.bind(function() {
                        $(this).remove();
                    }, this));
                }
                c++;

            });
            while (c <= number) {
                var teilnemherRow = $(teilnehmerTemplate({c: c})).hide();

                teilnemherRow.find('input.childcheckbox'+c).change(function() {
                    var $panrInput = $('.panr-'+this.className);
                    if ($(this).is(':checked')) {
                        $panrInput.data('value', $panrInput.val());
                        $panrInput.val('');
                        $panrInput.attr('placeholder', 'wird nicht benötigt');
                        $panrInput.attr('disabled', 'disabled');
                        $panrInput.removeAttr('required');
                    } else {
                        $panrInput.val($panrInput.data('value'));
                        $panrInput.attr('placeholder', 'Personalausweis Nummer');
                        $panrInput.attr('required', 'required');
                        $panrInput.removeAttr('disabled');
                    }
                });
                $teilnehmerContainer.append(teilnemherRow);



                teilnemherRow.slideDown('fast');
                c++;
            }
        }
    }

    var message = restkapazitaetC==1?'Nur noch ein Platz verfügbar':'Nur noch '+restkapazitaetC+' Plätze verfügbar';

    $.validator.addMethod("maximumV1", function(value, element) {
        var $element1 = $('#teilnehmerCount');
        var $element2 = $('#childrenCount');
        var count = parseInt($element1.val());
        if ($element2.length > 0) {
            count += parseInt($element2.val());
        }
        return count <= restkapazitaetC;

    }, message);

    $.validator.addMethod("validatePhone", function(value, element) {
        const regex1 = /[0-9]{3,}/g;
        const regex2 = /[a-zA-Z]+/g;
        const str = value;
        var m;
        var n;

        var thereAreNumbers = false;
        while ((m = regex1.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex1.lastIndex) {
                regex1.lastIndex++;
            }
            thereAreNumbers = true;
        }

        var thereAreChars = false;
        while ((n = regex2.exec(str)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (n.index === regex2.lastIndex) {
                regex2.lastIndex++;
            }
            thereAreChars = true;
        }

        return thereAreNumbers && (!thereAreChars);

    }, 'Bitte geben Sie eine gültige Telefonnummer ein');

    $("#bookingForm").validate();
});