<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'JAKOTA.' . $_EXTKEY,
	'HansesailBooking',
	array(
		'Booking' => 'list, form, crmError, shipToProduct, teaser, sofortNotifications, sofortOk, sofortAbort, pdf',

	),
	// non-cacheable actions
	array(
		'Booking' => 'form, shipToProduct, sofortNotifications',
	)
);

$composerAutoloadFile = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY)
	. 'Resources/Private/PHP/sofortlib/vendor/autoload.php';

require_once($composerAutoloadFile);


// eID
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['hansesailBookingSofortNotification'] = 'EXT:'.$_EXTKEY.'/Classes/Utility/Eid/SofortNotifications.php';